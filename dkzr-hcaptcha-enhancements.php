<?php
/**
 * Plugin Name: dkzr hCaptcha enhancements
 * Requires Plugins: hcaptcha-for-forms-and-more
 * Plugin URI: https://dkzr.nl
 * Update URI: https://api.dkzr.nl/wp/update-check/
 * Composer Package Name: dkzr/hcaptcha-enhancements
 * Description: Allow for global hCaptcha settings using constants in wp-config. Eg. define <em>string</em> constants <code>HCAPTCHA_SITE_KEY</code>, <code>HCAPTCHA_SECRET_KEY</code>, <code>HCAPTCHA_THEME</code>, <code>HCAPTCHA_SIZE</code> or <em>array</em> constants <code>HCAPTCHA_OFF_WHEN_LOGGED_IN</code> and <code>HCAPTCHA_WP_STATUS</code> etc.
 * Author: Joost de Keijzer
 * Version: 4.1.1
 */

class dkzrHcaptchaEnhancements {
  public function __construct() {
    add_action( 'admin_footer', [ $this, 'settings_page_hcaptcha' ], PHP_INT_MAX );
    add_filter( 'option_hcaptcha_settings', [ $this, 'option_hcaptcha_settings' ], PHP_INT_MAX, 1 );
    add_filter( 'site_option_hcaptcha_settings', [ $this, 'option_hcaptcha_settings' ], PHP_INT_MAX, 1 );
    add_filter( 'site_option_hcaptcha_settings_network_wide', [ $this, 'option_hcaptcha_settings_network_wide' ], PHP_INT_MAX, 1 );
  }

  public function option_hcaptcha_settings( $value ) {
    foreach( $value as $name => $setting ) {
      $constant = 'HCAPTCHA_' . strtoupper( str_replace( '-', '_', $name ) );
      if ( defined( $constant ) ) {
        $value[ $name ] = constant( $constant );
      }
    }

    return $value;
  }

  public function option_hcaptcha_settings_network_wide( $value ) {
    $constant = 'HCAPTCHA__NETWORK_WIDE';
    if ( defined( $constant ) ) {
      $value= constant( $constant );
    }

    return $value;
  }

  public function settings_page_hcaptcha() {
    $screen = get_current_screen();
    if (
      $screen->id == 'settings_page_hcaptcha'
      || str_starts_with( $screen->id, 'settings_page_hcaptcha-' )
      || $screen->id == 'toplevel_page_hcaptcha'
      || str_starts_with( $screen->id, 'toplevel_page_hcaptcha-' )
      || str_starts_with( $screen->id, 'hcaptcha_page_hcaptcha-' )
    ) {
      wp_enqueue_script( 'jquery' );
      $disable = [];
      if ( is_multisite() && get_site_option( 'hcaptcha_settings_network_wide', [] ) ) {
        $options = get_site_option( 'hcaptcha_settings', [] );
      } else {
        $options = get_option( 'hcaptcha_settings', [] );
      }
      foreach( $options as $name => $settings ) {
        $constant = 'HCAPTCHA_' . strtoupper( str_replace( '-', '_', $name ) );
        if ( defined( $constant ) ) {
          $disable[] = "[name=\"hcaptcha_settings[{$name}]\"]";
          $disable[] = "[name=\"hcaptcha_settings[{$name}][]\"]";
        }
      }
      $disable = implode( ',', $disable );
      echo <<<EOJ
<script type="text/javascript">
  jQuery( 'input[type=password]' ).attr( { autocomplete: 'off', spellcheck: false } ).parent( 'form' ).attr( 'autocomplete', 'off' );
  jQuery( '$disable' ).attr( { disabled: 'disabled', readonly: 'readonly' } ).closest( 'fieldset' ).attr( 'disabled', 'disabled' );
  jQuery( 'input[type=text], input[type=password], select, textarea' ).attr( 'title', function() {
    return 'Constant name: "HCAPTCHA_' + this.name.slice(18, -1).replace('-', '_').toUpperCase() + '" (string value)';
  } );
  jQuery( 'input[type=number]' ).attr( 'title', function() {
    return 'Constant name: "HCAPTCHA_' + this.name.slice(18, -1).replace('-', '_').toUpperCase() + '" (numeric value)';
  } );
  jQuery( 'input[type=checkbox]' ).each( function() {
    var input = this;
    jQuery( input ).closest( 'label' ).attr( 'title', function() {
      return 'Constant name: "HCAPTCHA_' + input.name.replace('[]', '').slice(18, -1).replace('-', '_').toUpperCase() + '"\\nArray value: "' + input.value + '"';
    } );
  } );
</script>
EOJ;

      printf(
        '<div class="notice notice-info"><p>%s<br>%s</p></div>',
        esc_html__( 'The disabled settings are configured using constants and the dkzr hCaptcha enhancement plugin', 'dkzr-hcaptcha-enhancements' ),
        esc_html__( 'Hover the element to see the constant name to use in wp-config.', 'dkzr-hcaptcha-enhancements' )
      );
    }
  }
}
$dkzrHcaptchaEnhancements = new dkzrHcaptchaEnhancements();
